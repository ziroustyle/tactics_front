import { Component, OnInit } from '@angular/core';
import {BackendService} from '../../services/backend.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  username = '';
  password = '';
  repassword = '';

  constructor(private backend: BackendService, private router: Router) { }

  ngOnInit() {
  }

  async signUp() {
    try {
      const data = await this.backend.signUp(this.username, this.password, this.repassword);

      await this.router.navigateByUrl('/login');
    } catch (e) {
      console.log(e);
      alert(e.error);
    }
  }

}
