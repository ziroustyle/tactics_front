import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UserService} from './user.service';
import {AuthenticationService} from './authentication.service';

export const httpOptions = ({
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': ''
  })
});
@Injectable({
  providedIn: 'root'
})
export class BackendService {

  private URL = 'http://127.0.0.1:8080/';

  constructor(private http: HttpClient, private userService: UserService, private auth: AuthenticationService) {
  }

  async logIn(user: string, pass: string): Promise<any> {
    const body = {user: user, password: pass};
    return await this.http.post(this.URL + 'login', JSON.stringify(body)).toPromise();
  }

  async signUp(user: string, pass: string, repass: string): Promise<any> {
    const body = {user: user, password: pass, repassword: repass};
    return await this.http.post(this.URL + 'signup', JSON.stringify(body)).toPromise();
  }

  async getCurrentPlayer(): Promise<any> {
    const body = {id: this.userService.getUserId()};
    return await this.http.post(this.URL + 'get_current_player', JSON.stringify(body)).toPromise();
  }

  async createNewHero(nickname: string, race: string, job: string): Promise<any> {
    const body = {id: this.userService.getUserId(), nickname: nickname, race: race, job: job};
    return await this.http.post(this.URL + 'new_hero', JSON.stringify(body)).toPromise();
  }

  async getTeam(id: string): Promise<any> {
    return Promise;
  }

  async getAllRaces(): Promise<any> {
    return await this.http.get(this.URL + 'get_all_races').toPromise();
  }

}
