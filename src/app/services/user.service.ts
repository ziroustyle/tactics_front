import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userId: Subject<string | null> = new Subject();
  
  constructor() { }

  setUserId(id: any) {
    this.userId = id;
  }

  getUserId() {
    return this.userId;
  }

  asObservable(): Observable<string | null> {
    return this.userId.asObservable();
  }
}
