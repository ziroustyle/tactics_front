import {Component, Input, OnInit} from '@angular/core';
import {BackendService} from '../../../services/backend.service';

@Component({
  selector: 'app-create-hero',
  templateUrl: './create-hero.component.html',
  styleUrls: ['./create-hero.component.scss']
})
export class CreateHeroComponent implements OnInit {

  @Input() open = false;

  races: any = [];
  currentRace: any | null;
  currentJob: any | null;
  nickname: string;

  constructor(private backend: BackendService) { }

  async ngOnInit() {
    await this.getAllRaces();
  }

  async getAllRaces() {
    try {
      const data = await this.backend.getAllRaces();
      this.races = await JSON.parse(data);
      this.currentRace = await this.races[0];
      this.currentJob = await this.currentRace.jobs[0];
    } catch (e) {
      console.log(e.error);
    }
  }

  selectRace(race) {
    this.currentRace = race;
    this.currentJob = this.currentRace.jobs[0];
  }

  selectJob(job) {
    this.currentJob = job;
  }

  async createHero() {
    try {
      await this.backend.createNewHero(this.nickname, this.currentRace, this.currentJob);
    } catch (e) {
      console.log(e.error);
    }
  }

}

