import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService implements CanActivate {

  private token: Subject<string | null> = new Subject();

  constructor(private router: Router) { }

  canActivate() {
    return this.isAuthenticated();
  }

  async isAuthenticated(): Promise<boolean> {
    if (this.getToken()) {
      return true;
    } else {
      await this.router.navigateByUrl('/login');
      return false;
    }
  }

  setToken(token: string) {
    localStorage.setItem('auth', token);
    this.token.next(token);
  }

  getToken(): string | null {
    return localStorage.getItem('auth');
  }

  removeToken() {
    localStorage.removeItem('auth');
  }

  asObservable(): Observable<string | null> {
    return this.token.asObservable();
  }
}
