import { Component, OnInit } from '@angular/core';
import {BackendService} from '../../services/backend.service';
import {AuthenticationService} from '../../services/authentication.service';
import {Router} from '@angular/router';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  private username: string | null = null;
  private password: string | null = null;
  private audio = new Audio();

  constructor(private backend: BackendService,
              private auth: AuthenticationService,
              private userService: UserService,
              private router: Router) { }

  ngOnInit() {

  }

  async login() {
    try {
      const data = await this.backend.logIn(this.username, this.password);
      this.auth.setToken(data['token']);
      this.userService.setUserId(data['id']);
      await this.router.navigateByUrl('/menu');
      this.audio.pause();
    } catch (e) {
        alert(e.error);
    }
  }

  async playBgSound() {
    this.audio = new Audio();
    this.audio.src = '../../../assets/login/audio/fairy.wav';
    this.audio.load();
    await this.audio.play();
  }

}
