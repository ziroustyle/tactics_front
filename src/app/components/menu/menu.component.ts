import { Component, OnInit } from '@angular/core';
import {BackendService} from '../../services/backend.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  user: any;

  constructor(private backend: BackendService) {
  }

  async ngOnInit() {
    try {
      this.user = await this.backend.getCurrentPlayer();
      console.log(this.user);
    } catch (e) {
      console.log(e);
    }
  }

}
